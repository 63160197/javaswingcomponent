/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.javaswingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Nippon
 */
public class SwingIntroduction3 extends JFrame {

    JFrame f;

    SwingIntroduction3() {
        JButton b = new JButton("click");
        b.setBounds(130, 100, 100, 40);

        add(b);
        setSize(400, 500);
        setLayout(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        new SwingIntroduction3();
    }
}
